#Overview
miki-set-ear 
	Combine ejb,jpa and web deploy to weblogic.
miki-set-ejb
	StockResultRepository.findAll to Return latest StockResult from StockResult Table.
miki-set-jpa
	Represent StockResult Table.
miki-set-retriever
	Periodically every 5 minutes to get from SETTRADE endpoint and use XPATH/REGEX to parse to StockResult entity.
	See Scheduler class to execute.
miki-set-web
	Invoke StockResultRepository.findAll through endpoint URI_HOME/StockResultService/getAll to get latest result and show on table.

#Not Finished Tasks
1. Create StockResult Table.
	STOCKIDX VARCHAR(100) 
	STOCKLAST NUMBER
2. miki-set-retriever
	Create Parser and slice result to StockResult entity and insert/update into StockResult Table.
		- Parser should be XPath / REGEX
3. miki-set-web
	Create web page to invoke web service and display result.

